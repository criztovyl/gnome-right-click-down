# Right Click Down

Send windows one workspace down by rightclicking them in the overview.

Settings:

-  `org/gnome/shell/extensions/right-click-down/close-overview`: Close overview after click.
